Create a wiki for your gitlab project easely using gitlab pages and vuepress.

# usage

- create `wiki/` in root directory
- include vuepress-wiki-ci.yml:
```yml
include: 'https://gitlab.com/quick-ci/vuepress-wiki/vuepress-wiki-ci.yml'
```