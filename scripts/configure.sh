#!/bin/sh

if [ ! -f "${WIKI_SOURCE_DIR}/package.json" ]; then
    wget -O ${WIKI_SOURCE_DIR}/package.json https://gitlab.com/quick-ci/vuepress-wiki/-/raw/main/package.json
fi

if [ ! -d "${WIKI_SOURCE_DIR}/.vuepress" ]; then
    mkdir ${WIKI_SOURCE_DIR}/.vuepress
fi

if [ ! -f "${WIKI_SOURCE_DIR}/.vuepress/config.yml" ]; then
    wget -O wiki_default_conf.yml https://gitlab.com/quick-ci/vuepress-wiki/-/raw/main/defaults/config.yml
    envsubst < wiki_default_conf.yml > ${WIKI_SOURCE_DIR}/.vuepress/config.yml
    rm wiki_default_conf.yml
fi